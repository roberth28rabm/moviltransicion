//Librerias importadas
import 'package:flutter/material.dart';

//Clase Spinner
class SpinnerWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    //Centrar el objeto
    return Center(
      child: Container(
        //Alinia al objeto
        alignment: AlignmentDirectional.bottomCenter,
        child: Column(
          children: <Widget>[
            CircularProgressIndicator()
          ],
          //Alineación 
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        )
      ),
    );
  }
}