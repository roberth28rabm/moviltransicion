//Importar librerias
import 'package:flutter/material.dart';
import 'package:examen2p/models/movie.dart';
import 'package:examen2p/ui/cajapelicula.dart';


// Clase pelicula 
class Pelicula extends StatelessWidget{

  //Objeto de la clase movie
  Movie movie;

  //Contructo para definir que el parametro es obligatorio
  Pelicula({required this.movie}): super();

  @override
  Widget build(BuildContext context) {
    //Widget de tipo Scaffold
    return Scaffold(
      appBar: AppBar(
        title: Text(this.movie.title),
      ),
      body: ListView(
        //
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          //Imagen de la pelicula
          Image.network(this.movie.getImage()),
          //Resumen de la pelicula
          Text(this.movie.overview)
        ],
      )
    );
  }

}
