//Librerias importadas 
import 'package:examen2p/models/movie.dart';

//Clase movie 
class Movies {

  Movies();
  //Este contructor sirve para extraer los datos json a una lista en flutter
  static List<Movie> fromJsonList(List<dynamic> jsonList) {
    List<Movie> listaPeliculas = [];

    //Por medio de esta consulta se verifica si jsonList esta vacia 
    if (jsonList != null) {
      for (var pelicula in jsonList) {
        //Extrae la pelicula
        final movie = Movie.fromJson(pelicula);
        //Inserta la pelicula
        listaPeliculas.add(movie);
      }
    }
    //Devuelve la lista de peliculas 
    return listaPeliculas;
  }
}