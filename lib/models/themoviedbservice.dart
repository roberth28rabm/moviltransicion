//Definir servicio de peliculas

//Librerias importadas
import 'package:examen2p/models/movie.dart';
import 'package:examen2p/models/movies.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

//Clase para definir el servicio de las peliculas

class TheMovieDBService {

  //LLamada al api de movie 
  static final String API_url = "https://api.themoviedb.org/3";

  //Definir la clave estatica del api movie 
   static final String API_key = "99b4c3070137e46945a47d68eb4d49f3";

  //Clase asincrona con FUTURE
  static Future<List<Movie>> getTopRatedMovies() async {

    //Esta llamade extrae los datos y los procesa 
    var url = API_url + "/movie/top_rated?api_key=" + API_key + "&language=es";
    //Llama de manera asincronica al metodo http get de la url 
    final respuesta = await http.get(Uri.parse(url));

    //Comprueba si existe respuesta por medio del status code 200
    if (respuesta.statusCode == 200) {
      //La respuesta se almacena en respuestaJSON
      final respuestaJSON = json.decode (respuesta.body);
      //Procesa con Movies.fromJsonList la respuesta recibida
      final peliculasMejorValoradas = Movies.fromJsonList(respuestaJSON['results']);
      //Retorna la pelicula mejor valorada
      return peliculasMejorValoradas;
    }

    //En caso de existir respuesta y no devuelva datos 
    return <Movie>[];
  }

}
